/**
 * @file
 * Classie - class helper functions from bonzo https://github.com/ded/bonzo.
 *
 * Classie.has( elem, 'my-class' ) -> true/false.
 * Classie.add( elem, 'my-new-class' ).
 * Classie.remove( elem, 'my-unwanted-class' ).
 * Classie.toggle( elem, 'my-class' ).
 */

(function(window) {

  'use strict';

  // Class helper functions from bonzo https://github.com/ded/bonzo.
  function classReg(className) {
    return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
  }

  // ClassList support for class management Although to be fair,
  // the api sucks because it won't accept multiple classes at once.
  var hasClass, addClass, removeClass;

  if ('classList' in document.documentElement) {
    hasClass = function(elem, c) {
      return elem.classList.contains(c);
    };
    addClass = function(elem, c) {
      elem.classList.add(c);
    };
    removeClass = function(elem, c) {
      elem.classList.remove(c);
    };
  }
  else {
    hasClass = function(elem, c) {
      return classReg(c).test(elem.className);
    };
    addClass = function(elem, c) {
      if (!hasClass(elem, c)) {
        elem.className = elem.className + ' ' + c;
      }
    };
    removeClass = function(elem, c) {
      elem.className = elem.className.replace(classReg(c), ' ');
    };
  }

  function toggleClass(elem, c) {
    var fn = hasClass(elem, c) ? removeClass : addClass;
    fn(elem, c);
  }

  var classie = {
    // Full names.
    hasClass: hasClass,
    addClass: addClass,
    removeClass: removeClass,
    toggleClass: toggleClass,
    // Short names.
    has: hasClass,
    add: addClass,
    remove: removeClass,
    toggle: toggleClass
  };

  if (typeof define === 'function' && define.amd) {
    define(classie);
  }
  else {
    window.classie = classie;
  }

})(window);
